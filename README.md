# Parcel, Elm, TailwindCSS ++ Docker template project

## Up and Running

Development container:
- `pnpm up:dev`

Local development instance:
- `pnpm install`
- `pnpm postinstall`
- `pnpm serve`

## License

[MIT](https://choosealicense.com/licenses/mit/)
