# Production Stage
FROM node:20-alpine as prod-stage
LABEL authors="anthony"

WORKDIR /app

COPY ["package.json", "elm*.json", "./"]

RUN npm i -g pnpm
RUN pnpm i
RUN pnpm prebuild

COPY . .

RUN pnpm build

EXPOSE 1234

CMD ["pnpm", "serve"]