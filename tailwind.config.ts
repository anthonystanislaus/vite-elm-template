/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: "jit",
  content: [
      "./index.html",
      "./src/**/*.{elm,js,html}"
  ],
  theme: {
    extend: {
      colors: {
        white: "hsl(var(--color-text-primary) / <alpha-value>)",
        lightYellow: "hsl(60, 100%, 98%)",
        yellow: "hsl(60, 100%, 72%)",
        gray: "hsl(var(--color-text-secondary) / <alpha-value>)",
        background: "hsl(0, 0%, 2%)"
      },
      fontFamily: {
        primary: [ "Inconsolata", "Ligconsolata", "Consolas", "Arial" ],
        secondary: [ "'JetBrains Mono'" ]
      },
    },
  },
  plugins: [],
  variants: {},
}
