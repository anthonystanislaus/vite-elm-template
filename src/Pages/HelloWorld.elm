module HelloWorld exposing (helloWorld)

import Html exposing (Html, div, h1, img, text)
import Html.Attributes exposing (class, src)


helloWorld : Html msg
helloWorld =
    div []
        [ h1 [ class "text-3xl font-bold underline" ]
            [ text "Hello, Parcel ++ Elm ++ TailwindCSS ++ Docker" ]
        , img [ src "src/assets/logo.png" ]
            []
        ]
