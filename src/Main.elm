module Main exposing (Model, Msg(..), main)

import Browser exposing (Document, UrlRequest(..), application)
import Browser.Navigation exposing (Key, load, pushUrl)
import Html exposing (Html, a, b, li, text, ul)
import Html.Attributes exposing (href)
import Url exposing (Url)



-- TYPES


type Msg
    = UrlRequested UrlRequest
    | UrlChanged Url


type alias Model =
    { url : Url
    , key : Key
    }



-- MODEL


init : () -> Url -> Key -> ( Model, Cmd Msg )
init _ url key =
    ( Model url key, Cmd.none )



-- VIEW


view : Model -> Document Msg
view model =
    { title = "537D"
    , body =
        [ text "The current URL is: "
        , b [] [ text <| Url.toString model.url ]
        , ul []
            [ viewLink "/home"
            , viewLink "/about"
            , viewLink "/projects"
            , viewLink "/availability"
            ]
        ]
    }


viewLink : String -> Html msg
viewLink path =
    li [] [ a [ href path ] [ text path ] ]



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlRequested urlRequest ->
            case urlRequest of
                Internal url ->
                    ( model, pushUrl model.key (Url.toString url) )

                External href ->
                    ( model, load href )

        UrlChanged url ->
            ( { model | url = url }
            , Cmd.none
            )



-- MAIN


main : Program () Model Msg
main =
    application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = UrlRequested
        }


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
