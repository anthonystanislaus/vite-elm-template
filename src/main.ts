// @ts-ignore
import {Elm} from "./Main.elm";
import "../public/index.css";

const root = document.querySelector("#app");

Elm.Main.init({ node: root });
